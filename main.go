package main

import (
	"fmt"

	"gitlab.com/mvkan/gitlab-ci-cd-101/pkg/example"
)

func main() {
	fmt.Println(example.Hello("John"))
}
