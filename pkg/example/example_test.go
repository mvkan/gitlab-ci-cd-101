package example_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mvkan/gitlab-ci-cd-101/pkg/example"
)

func TestExample_Hello(t *testing.T) {
	name := "John"
	rightString := "Hello, " + name
	testString := example.Hello("John")
	assert.Equal(t, testString, rightString)
}
